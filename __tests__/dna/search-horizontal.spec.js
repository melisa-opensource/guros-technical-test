const dna = require('../../src/dna')

describe('search mutations horizontal', () => { 
   
  it('mutation horizontal found', () => {
    const input = [
      'AAAA',
      'CTAA',
      'ACTG',
      'ACAA'
    ]
    expect(dna(input)).toBe(1)
  })

  it('mutation horizontal with max mutations', () => {
    const input = [
      'AAAA',
      'AAAA',
      'AAAA',
      'CTGC'
    ]
    expect(dna(input, {
      maxMutations: 2
    })).toBe(2)
  })

  it('mutation horizontal with max mutations cero', () => {
    const input = [
      'AAAA',
      'AAAA',
      'AAAA',
      'CTGC'
    ]
    expect(dna(input, {
      maxMutations: 0,
      searchVertical: false
    })).toBe(3)
  })

  it('mutation horizontal with max mutations case 2', () => {
    const input = [
      'AAAAACGCC',
      'CAAAAAGTT',
      'TTAAAAATT',
      'AGAAAAAAA',
      'TTATATATT',
      'AGACAAACA',
      'TTTTATTTT',
      'AGACACAAA',
      'AGACACAAA'
    ]
    expect(dna(input)).toBe(2)
    expect(dna(input, {
      maxMutations: 6
    })).toBe(6)
    expect(dna(input, {
      maxMutations: 3
    })).toBe(3)
    expect(dna(input, {
      maxMutations: 0,
      searchVertical: false
    })).toBe(13)
  })

})
