const dna = require('../../src/dna')

describe('search mutations vertical', () => { 
   
  it('mutation vertical found', () => {
    const input = [
      'ATAA',
      'CTGA',
      'AATA',
      'ACAA'
    ]
    expect(dna(input, {
      searchHorizontal: false
    })).toBe(1)
  })

  it('mutation vertical with max mutations', () => {
    const input = [
      'AAAT',
      'AAAC',
      'AAAG',
      'AAAT'
    ]
    expect(dna(input, {
      maxMutations: 2
    })).toBe(2)
  })

  it('mutation vertical with max mutations case 2', () => {
    const input = [
      'AAAAACGCC',
      'CAAAAAGTT',
      'TTAAAAATT',
      'AGAAAAAAA',
      'TTATATATT',
      'AGACAAACA',
      'TTTTATTTA',
      'AGACACAAA',
      'AGACACAAA'
    ]
    
    expect(dna(input, {
      searchHorizontal: false,
      serchVertical: false,
      searchObliqueLeftToRight: false,
      searchObliqueRightToLeft: false
    })).toBe(2)
    
    expect(dna(input, {
      maxMutations: 5,
      serchVertical: false,
      searchHorizontal: false,
      searchObliqueLeftToRight: false,
      searchObliqueRightToLeft: false
    })).toBe(5)

    expect(dna(input, {
      maxMutations: 0
    })).toBe(18)
    
  })
})
