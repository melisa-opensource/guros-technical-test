const dna = require('../../src/dna')

describe('validate input', function () {

  it('input string is invalid', () => {
    const input = ''
    expect(dna(input)).toBe(false)
  })

  it('input object is invalid', () => {
    const input = {}
    expect(dna(input)).toBe(false)
  })

  it('min columns valid', () => {
    const input = [
      'AAA',
      'CTAA',
      'ACTG',
      'ACAA'
    ]
    expect(dna(input)).toBe(false)
  })

  it('invalid columns', () => {
    const input = [
      'AAAAA',
      'CTAA',
      'ACTG',
      'ACAA'
    ]
    expect(dna(input)).toBe(false)
  })

  it('invalid matrix NxN', () => {
    const input = [
      'AAAA',
      'CTAA',
      'ACTG',
      'ACAA',
      'ACAA'
    ]
    expect(dna(input)).toBe(false)
  })

  it('invalid characters', () => {
    const input = [
      'aaaa',
      'ctaa',
      'bCTG',
      'xyAA'
    ]
    expect(dna(input)).toBe(false)
  })

  it('valid matrix NxN', () => {
    const input = [
      'AAAA',
      'CTAA',
      'ACTG',
      'ACAA'
    ]
    expect(dna(input)).toBe(1)
  })
})
