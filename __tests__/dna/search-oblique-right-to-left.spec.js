const dna = require('../../src/dna')

describe('search mutations oblique right to left', () => { 
   
  it('mutation oblique found right to left', () => {
    const input = [
      'ATAA',
      'CAAA',
      'AACC',
      'ACAA'
    ]
    expect(dna(input)).toBe(1)
  })

  it('mutation oblique found right to left case 2', () => {
    const input = [
      'AAAAACGCC',
      'CAAAAAGTT',
      'TTAAAAATT',
      'AGAAAAAAA',
      'TTATATATT',
      'AGACAAACA',
      'TTTTATTTA',
      'AGACACAAA',
      'AGACACAAA'
    ]

    expect(dna(input, {
      maxMutations: 0,
      searchHorizontal: false,
      searchVertical: false,
      searchObliqueLeftToRight: false
    })).toBe(2)

    expect(dna(input, {
      maxMutations: 0
    })).toBe(18)
  })

})
