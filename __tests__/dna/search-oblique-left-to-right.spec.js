const dna = require('../../src/dna')

describe('search mutations oblique left to right', () => { 
   
  it('mutation oblique found left to right', () => {
    const input = [
      'ATAA',
      'CAGA',
      'ATAC',
      'ACAA'
    ]
    expect(dna(input, {
      searchHorizontal: false,
      searchVertical: false
    })).toBe(1)
  })

  it('mutation oblique found left to right with max mutations', () => {
    const input = [
      'AAAAACGCC',
      'CAAAAAGTT',
      'TTAAAAATT',
      'AGAAAAAAA',
      'TTATATATT',
      'AGACAAACA',
      'TTTTATTTA',
      'AGACACAAA',
      'AGACACAAA'
    ]
    expect(dna(input, {
      searchHorizontal: false,
      searchVertical: false
    })).toBe(2)

    expect(dna(input, {
      maxMutations: 5,
      searchHorizontal: false,
      searchVertical: false,
      searchObliqueRightToLeft: false
    })).toBe(5)

    expect(dna(input, {
      maxMutations: 3,
      searchHorizontal: false,
      searchVertical: false,
      searchObliqueRightToLeft: false
    })).toBe(3)
    
    expect(dna(input, {
      maxMutations: 0
    })).toBe(18)
  })

})
