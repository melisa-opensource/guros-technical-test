# Guros Technical Test

Este proyecto contiene una solución a la prueba técnica:

"Requerimos que desarrolles un proyecto que detecte si una persona tiene diferencias genéticas basándose en
su secuencia de ADN."

Más detalle sobre la prueba consultar el README ubicado en la carpeta [docs](docs/README.md).

Enlaces de la funcionalidad:

1. [URL a la API](https://cppz8j2ir8.execute-api.us-east-1.amazonaws.com/stage/)
1. [Ejecución de Prueba unitaria](https://youtu.be/UytDFmOUq3A)
1. [Despliegue con Pulumi](https://youtu.be/r57hvos7KCk)

## Estructura del proyecto

El proyecto esta desarrollado completamente en Javascript sobre [NodeJS](https://nodejs.org/en/) y se puede dividir en 3 partes:

1. Programa que detecta mutaciones en ADN
1. Pruebas unitarias usando [Jest](https://jestjs.io/)
1. Despliegue de API en AWS desarrollado con [Pulumi](https://www.pulumi.com/)
1. API Postman la puede encontrar dentro de la carpeta docs

## Requisitos para su funcionamiento

1. Tener instalado Node JS, Jest y Pulumi (opcional) 
1. Instalar las dependencias
    ```sh
    npm i
    ```
1. Opcionalmente, siga las instrucciones de la sección **Despliegue de API en AWS**


## Programa que detecta mutaciones en ADN

El programa esta ubicado en src/dna.js. Tal script solo exporta el metodo hasMutations.
El script se detiene si encuentra 2 mutaciones. Ademas, si es necesario se puede pasar como segundo argumento un objecto, un ejemplo con las configuraciones soportadas son:

```js
{
    // por default 2, si es necesario no se detiene al encontrar más de 2
    maxMutations: 0,
    // si es false no realiza la busqueda horizontal
    searchHorizontal: true,
    // mismo caso que el anterior pero vertical
    searchVertical: true,
    // y asi sucesivamente, ningun parametro es requerido, por default todos son true, excepto logger
    searchObliqueLeftToRight: true,
    searchObliqueRightToLeft: true,
    logger: false
}
```

## Ejecutar pruebas untarias

```sh
npm run test
```

## Despliegue de API en AWS

Si lo desean pueden desplegar la API sobre AWS usando infraestructura como código. Con ayuda de Pulumi es posible desplegar lo siguiente en AWS:

1. API Gateway que enruta peticiones hacia lambdas
1. Lambdas functions
1. Layers que son paquetes que son dependencias enlazadas a las lambdas
1. Base de datos Dynamodb que permite almacenar los ADNs y la estadistica

Los requisitos para desplegar son:

1. Tener instalado [Pulumi](https://www.pulumi.com/docs/get-started/aws/begin/)

1. Se require crear usuario en AWS IAM de tipo "Programmatic access" y obtener el AWS_ACCESS_KEY_ID y el AWS_SECRET_ACCESS_KEY.

3. Ejecutar el siguiente comando

```sh
export AWS_ACCESS_KEY_ID=AKIAVXO5RXUE3HGE2Y7G
export AWS_SECRET_ACCESS_KEY=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
```

4. Asignarle al usuario creado los siguientes permisos:

> ***Nota:*** No es buena practica asignar permisos Full Access.

```json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": [
                "iam:*",
                "s3:*",
                "apigateway:*",
                "logs:*",
                "lambda:*",
                "dynamodb:*"
            ],
            "Resource": "*"
        }
    ]
}
```

### Desplegar la solución a AWS

```sh
npm run deploy
```

Al desplegar satisfactoriamente se le mostrara como resultado la dirección a la API generada.

El archivo index.js contiene la configuración base para que pulumi haga su magia. Depende de todos los archivos ubicados en la carpeta pulumi.

En caso de haber desplegado el proyecto puede ejecutar el siguiente comando:

```sh
npm run destroy
```

Con esto elimina todos los recursos creados.

# Documentación y/o herramientas de apoyo para resolver problema

1. [Ejemplos de casos](https://docs.google.com/spreadsheets/d/1zFYxSPNNdKtvuoy-245YwdLlB7UuD10dD-QpsLz8cJQ/edit#gid=0)
1. [RunJS](https://runjs.app/)
1. [Regex101](https://regex101.com/)
1. Pulumi
    1. [Crear BD Dynamodb](https://www.pulumi.com/docs/reference/pkg/aws/dynamodb/table/)
1. Lambdas
    1. [Lambda Status Code](https://docs.aws.amazon.com/apigateway/latest/developerguide/handle-errors-in-lambda-integration.html)
1. Dynamodb
    1. [SDK Dynamodb](https://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/DynamoDB.html)
    1. [Actualizar registro](https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/Expressions.UpdateExpressions.html#Expressions.UpdateExpressions.SET)

