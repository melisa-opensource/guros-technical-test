"use strict";
const pulumi = require('@pulumi/pulumi')
const { dbStats, dbDnas } = require('./pulumi/database')
const { api, layer } = require('./pulumi/api')

exports.api = api.url
exports.layer = layer.arn
exports.dbStats = dbStats.arn
exports.dbDnas = dbDnas.arn
