const LETTERS = ['A', 'T', 'C', 'G']
let config = {}
let mutationsFound = 0

const initConfig = (customConfig) => {
  return {
    maxMutations: 2,
    searchHorizontal: true,
    searchVertical: true,
    searchObliqueLeftToRight: true,
    searchObliqueRightToLeft: true,
    logger: false,
    ...customConfig
  }
}

const logger = (params) => {
  if (!config.logger) {
    return
  }
  console.log.call(console, params)
}

const isValidInput = dna => {
  if (!Array.isArray(dna)) {
    return false
  }
  const minLetters = LETTERS.length
  const firstItem = dna[0].length
  if (firstItem < minLetters) {
    logger('min letters in first item')
    return false
  }
  if (firstItem !== dna.length) {
    logger('invalid matrix NxN')
    return false
  }
  const regex = new RegExp(/[A|T|C|G]/, 'gm')
  const totalItems = dna.length
  for (let index = 0; index < totalItems; index ++) {
    const result = dna[index].match(regex)
    if (!result || result.length !== totalItems) {
      logger('invalid characters')
      return false
    }
  }  
  return true
}

const incrementMutationAndBreak = (increment = 1) => {
  mutationsFound += increment
  if (!config.maxMutations) {
    logger('current mutations found', mutationsFound)
    return false
  }
  if (mutationsFound >= config.maxMutations) {
    logger('max mutations allowed', mutationsFound, config.maxMutations)
    return true
  }
  logger('current mutations found', mutationsFound)
  return false
}

const hasMutationHorizontal = dna => {
  const conditions = []
  for (const letter in LETTERS) {
    conditions.push(LETTERS[letter].repeat(4))
  }
  const regex = new RegExp(conditions.join('|'), 'g')
  const totalItems = dna.length
  for (let index = 0; index < totalItems; index ++) {
    const result = dna[index].match(regex)
    logger({
      index,
      result,
      dna: dna[index],
      increment: result ? result.length : 0
    })
    if (result === null) {
      continue
    }
    if (incrementMutationAndBreak(result.length)) {
      return true
    }
  }
  return false
}

const replaceString = (string, column) => {
  return string.substr(0, column) + 'x' + string.substr(column + 1)
}

const hasMutationVertical = dna => {
  const rows = dna.length - (LETTERS.length - 1)
  const columns = dna.length
  for (let row = 0; row < rows; row++) {
    for (let column = 0; column < columns; column ++) {
      const letter = dna[row][column]
      
      if (letter === 'x') {
        continue
      }
      
      const condition = dna[row + 1][column] === letter
        && dna[row + 2][column] === letter
        && dna[row + 3][column] === letter
      
      logger({
        letter,
        row,
        column,
        second: dna[row + 1],
        three: dna[row + 2][column],
        four: dna[row + 3][column],
        condition
      })
      
      if (condition) {
        dna[row + 1] = replaceString(dna[row + 1], column)
        dna[row + 2] = replaceString(dna[row + 2], column)
        dna[row + 3] = replaceString(dna[row + 3], column)
        logger(`mutation vertical found ${letter} in ${row} ${column}`)
        if (incrementMutationAndBreak()) {
          return true
        }
      }
    }
  }
  return false
}

const hasMutationObliqueLeftToRight = dna => {
  const rows = dna.length - (LETTERS.length - 1)
  const columns = dna.length - (LETTERS.length - 1)
  
  for (let row = 0; row < rows; row++) {
    for (let column = 0; column < columns; column ++) {
      const letter = dna[row][column]

      if (letter === 'x') {
        continue
      }
      
      const condition = dna[row + 1][column + 1] === letter
        && dna[row + 2][column + 2] === letter
        && dna[row + 3][column + 3] === letter
      
      logger({
        letter,
        row,
        column,
        second: dna[row + 1][column + 1],
        three: dna[row + 2][column + 2],
        four: dna[row + 3][column + 3],
        condition
      })
      
      if (condition) {
        dna[row + 1] = replaceString(dna[row + 1], column + 1)
        dna[row + 2] = replaceString(dna[row + 2], column + 2)
        dna[row + 3] = replaceString(dna[row + 3], column + 3)
        logger(`mutation oblique found left to right ${letter} in ${row} ${column}`)
        if (incrementMutationAndBreak()) {
          return true
        }
      }
    }
  }
  return false
}

const hasMutationObliqueRightToLeft = dna => {
  const rows = dna.length - (LETTERS.length - 1)
  const maxColumns = dna.length - 1
  const minColumns = dna.length === LETTERS.length ? 
    maxColumns - 1 : 
    LETTERS.length - 1
  
  for (let row = 0; row < rows; row++) {
    for (let column = maxColumns; column > minColumns; column --) {
      const letter = dna[row][column]

      if (letter === 'x') {
        continue
      }
      
      const condition = dna[row + 1][column - 1] === letter
        && dna[row + 2][column - 2] === letter
        && dna[row + 3][column - 3] === letter
      
      logger({
        letter,
        row,
        column,
        second: dna[row + 1][column - 1],
        three: dna[row + 2][column - 2],
        four: dna[row + 3][column - 3],
        condition
      })
      
      if (condition) {
        dna[row + 1] = replaceString(dna[row + 1], column - 1)
        dna[row + 2] = replaceString(dna[row + 2], column - 2)
        dna[row + 3] = replaceString(dna[row + 3], column - 3)
        logger(`mutation oblique found right to left ${letter} in ${row} ${column}`)
        if (incrementMutationAndBreak()) {
          return true
        }
      }
    }
  }
  return false
}

const hasMutation = (dna, customConfig = {}) => {
  config = initConfig(customConfig)
  mutationsFound = 0
  if (!isValidInput(dna)) {
    logger('invalid input dna')
    return false
  }
  if (config.searchHorizontal && hasMutationHorizontal(dna)) {
    return mutationsFound
  }
  if (config.searchVertical && hasMutationVertical([
    ...dna
  ])) {
    return mutationsFound
  }
  if (config.searchObliqueLeftToRight && hasMutationObliqueLeftToRight([
    ...dna
  ])) {
    return mutationsFound
  }
  if (config.searchObliqueRightToLeft && hasMutationObliqueRightToLeft([
    ...dna
  ])) {
    return mutationsFound
  }
  return mutationsFound
}

module.exports = hasMutation
