const AWS = require('aws-sdk')
const uuid = require('uuid')
const docClient = new AWS.DynamoDB.DocumentClient()

const createDna = params => {
  const item = {
    id: uuid.v4(),
    ...params
  }
  return docClient.put({
    TableName: 'guros2_dnas',
    Item: item
  }).promise()
}

const addDna = (params) => {
  return new Promise((resolve, reject) => {
    createDna(params).then(result => {
      console.log(result)
      updateStats(params.has_mutation === 's')
        .then(resolve)
        .catch(reject)
    }).catch(reject)
  })
}

const updateStats = hasMutations => {
  return new Promise((resolve, reject) => {
    getStats().then(result => {
      const fieldUpdate = hasMutations ? 
        'count_mutations' : 'count_no_mutation'
      let updateExpresion = [
        'SET ',
        fieldUpdate,
        ' = ',
        fieldUpdate,
        ' + :inc'
      ].join('')
      let ratio = result.ratio
      const countMutations = result.count_mutations + (hasMutations ? 1 : 0)
      const countNoMutations = result.count_no_mutation + (hasMutations ? 0 : 1)
      if (countMutations > 0 && countNoMutations > 0) {
        ratio = countMutations / countNoMutations
      }
      updateExpresion += ', ratio = :ratio'
      console.log('update expresion', updateExpresion)
      docClient.update({
        TableName: 'guros2_stats',
        Key: {
          id: 1
        },
        ExpressionAttributeValues: {
          ':inc': 1,
          ':ratio': ratio
        },
        UpdateExpression: updateExpresion
      }).promise()
        .then(resolve)
        .catch(reject)
    })
  })
}

const createStats = () => {
  return new Promise((resolve, reject) => {
    const item = {
      id: 1,
      count_mutations: 0,
      count_no_mutation: 0,
      ratio: 0
    }
    docClient.put({
      TableName: 'guros2_stats',
      Item: item
    }).promise().then(() => {
      delete item.id
      resolve(item)
    }).catch(reject)
  })
}

const getStats = () => {
  return new Promise((resolve, reject) => {
    docClient.get({
      TableName: 'guros2_stats',
      Key: {
        id: 1
      }
    }).promise().then(data => {
      if (typeof data.Item === 'undefined') {
        return createStats()
          .then(resolve)
          .catch(reject)
      }
      delete data.Item.id
      resolve({
        ...data.Item
      })
    }).catch(reject)
  })
}

const getBodyJson = event => {
  // support use test on web
  if (typeof event.body === 'undefined') {
    return event
  }
  // isBase64Encoded
  let inputString = event.body
  if (event.isBase64Encoded) {
    inputString = Buffer
      .from(inputString, 'base64')
      .toString('utf-8')
  }
  try {
    return JSON.parse(inputString)
  } catch (e) {
    console.error('invalid json')
    return null
  }
}

const getDefaultHeaders = () => {
  return {
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json'
  }
}

const responseSimpleJson = body => {
  return {
    statusCode: body ? 200 : 403,
    headers: getDefaultHeaders(),
    body: JSON.stringify(body)
  }
}

const responseJson = (data, errors) => {
  const success = data ? true : false
  let body = {
    success
  }
  if (success) {
    body.data = data
  } else {
    body.errors = errors
  }
  return {
    statusCode: success ? 200 : 403,
    headers: getDefaultHeaders(success),
    body: JSON.stringify(body)
  }
}

const debug = (params) => {
  console.log.call(console, params)
}

module.exports = {
  getStats,
  addDna,
  createDna,
  getBodyJson,
  responseJson,
  responseSimpleJson,
  debug
}
