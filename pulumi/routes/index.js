const mutation = require('./mutation')
const home = require('./home')
const stats = require('./stats')

module.exports = {
  mutation,
  home,
  stats
}
