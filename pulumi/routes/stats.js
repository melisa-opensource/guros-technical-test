module.exports = (req, ctx, cb) => {
  const lambda = require('app/lambda')
  lambda.getStats().then(result => {
    cb(null, lambda.responseSimpleJson(result))
  }).catch(error => {
    console.log(error)
    cb(null, lambda.responseJson(false, [
      {
        code: 'db.getStats'
      }
    ]))
  })
}
