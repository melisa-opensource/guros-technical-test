module.exports = (event, ctx, cb) => {
  const lambda = require('app/lambda')
  const dna = require('app/dna')
  const input = lambda.getBodyJson(event)
  const hasMutation = dna(input.dna || [], input.config || {})
  if (hasMutation === false) {
    return cb(null, lambda.responseJson(false))
  }
  lambda.addDna({
    dna: input.dna.join(','),
    has_mutation: hasMutation ? 's' : 'n',
    mutations: hasMutation
  }).then(() => {
    cb(null, lambda.responseJson(hasMutation ? {
      mutations: hasMutation
    } : false))
  }).catch(error => {
    console.log(error)
    cb(null, lambda.responseJson(false, [
      {
        code: 'db.save',
        message: error
      }
    ]))
  })
}
