module.exports = (req, ctx, cb) => {
  const lambda = require('app/lambda')
  cb(null, lambda.responseJson({
    pong: true
  }))
}
