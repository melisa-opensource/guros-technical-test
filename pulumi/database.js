"use strict";
const pulumi = require('@pulumi/pulumi')
const aws = require('@pulumi/aws')

const dbStats = new aws.dynamodb.Table('guros2_stats', {
  // necesary force name table
  name: 'guros2_stats',
  attributes: [
    {
      name: 'id',
      type: 'N'
    }
  ],
  hashKey: 'id',
  readCapacity: 1,
  writeCapacity: 1
})

const dbDnas = new aws.dynamodb.Table('guros2_dnas', {
  // necesary force name table
  name: 'guros2_dnas',
  attributes: [
    {
      name: 'id',
      type: 'S'
    }
  ],
  hashKey: 'id',
  readCapacity: 1,
  writeCapacity: 1
})

module.exports = {
  dbDnas,
  dbStats
}
