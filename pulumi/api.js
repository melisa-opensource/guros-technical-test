"use strict";
const pulumi = require('@pulumi/pulumi')
const awsx = require('@pulumi/awsx')
const aws = require('@pulumi/aws')
const routes = require('./routes/index')

const layer = new aws.lambda.LayerVersion('guros2-layer', {
  code: new pulumi.asset.AssetArchive({
    'nodejs/node_modules/app': new pulumi.asset.FileArchive('./src'),
    'nodejs/node_modules/uuid': new pulumi.asset.FileArchive('./node_modules/uuid')
  }),
  compatibleRuntimes: [
    aws.lambda.NodeJS12dXRuntime
  ],
  layerName: 'guros2_dna'
})

const api = new awsx.apigateway.API('guros2', {
  routes: [
    {
      path: '/',
      method: 'GET',
      eventHandler: new aws.lambda.CallbackFunction('home', {
        callback: routes.home,
        layers: [
          layer
        ]
      })
    },
    {
      path: '/mutation',
      method: 'POST',
      eventHandler: new aws.lambda.CallbackFunction('mutation', {
        callback: routes.mutation,
        layers: [
          layer
        ]
      })
    },
    {
      path: '/stats',
      method: 'GET',
      eventHandler: new aws.lambda.CallbackFunction('stats', {
        callback: routes.stats,
        layers: [
          layer
        ]
      })
    }
  ]
})

module.exports = {
  api,
  layer
}
